﻿using Patron.Facade.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckFacade facade1 = new CheckFacade();
            facade1.Buscar(new Models.Vuelo
            {
                FechaIda = "20/03/2019",
                FechaVuelta = "25/03/2019",
                Origen = "Querétaro",
                Destino = "Guadalajara"
            });

            CheckFacade facade2 = new CheckFacade();
            facade2.Buscar(new Models.Vuelo
            {
                FechaIda = "10/05/2019",
                FechaVuelta = "15/05/2019",
                Origen = "Querétaro",
                Destino = "México"
            });
            Console.ReadLine();
        }
    }
}
