﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Facade.Models
{
    public class Vuelo
    {
        public string FechaIda { get; set; }
        public string FechaVuelta { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }

        public Vuelo()
        {
            this.FechaIda = string.Empty;
            this.FechaVuelta = string.Empty;
            this.Origen = string.Empty;
            this.Destino = string.Empty;
        }
    }
}
