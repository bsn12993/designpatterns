﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Facade.Models
{
    public class Hotel
    {
        public string FechaIda { get; set; }
        public string FechaVuelta { get; set; }

        public Hotel()
        {
            this.FechaIda = string.Empty;
            this.FechaVuelta = string.Empty;
        }
    }
}
