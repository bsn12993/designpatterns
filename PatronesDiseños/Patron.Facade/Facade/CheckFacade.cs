﻿using Patron.Facade.Api;
using Patron.Facade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Facade.Facade
{
    public class CheckFacade
    {
        private AvionApi AvionApi;
        private HotelApi HotelApi;

        public CheckFacade()
        {
            this.AvionApi = new AvionApi();
            this.HotelApi = new HotelApi();
        }

        public void Buscar(Vuelo vuelo)
        {
            AvionApi.BuscarVuelos(vuelo);
            HotelApi.BuscarHoteles(new Hotel { FechaIda = vuelo.FechaIda, FechaVuelta = vuelo.FechaVuelta });
        }
    }
}
