﻿using Patron.Facade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Facade.Api
{
    public class AvionApi
    {
        public void BuscarVuelos(Vuelo vuelo)
        {
            Console.WriteLine("=======================");
            Console.WriteLine($"Vuelos encontrados para {vuelo.Destino} desde {vuelo.Origen}");
            Console.WriteLine($"Fecha IDA: {vuelo.FechaIda} Fecha Vuelta {vuelo.FechaVuelta}");
            Console.WriteLine("=======================");
        }
    }
}
