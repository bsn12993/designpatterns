﻿using Patron.Facade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Facade.Api
{
    public class HotelApi
    {
        public void BuscarHoteles(Hotel hotel)
        {
            Console.WriteLine("=======================");
            Console.WriteLine("Hoteles encontrados");
            Console.WriteLine($"Entrada: {hotel.FechaIda} Salida {hotel.FechaVuelta}");
            Console.WriteLine("Hotel A");
            Console.WriteLine("Hotel B");
            Console.WriteLine("Hotel C");
            Console.WriteLine("=======================");
        }
    }
}
