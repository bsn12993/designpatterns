﻿using Patron.Decorator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Decorator.Interfaz
{
    public interface ICuentaBancaria
    {
        void AbrirCuenta(Cuenta cuenta);
    }
}
