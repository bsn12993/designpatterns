﻿using Patron.Decorator.Interfaz;
using Patron.Decorator.Models;

namespace Patron.Decorator.Decorator
{
    public abstract class CuentaDecorador : ICuentaBancaria
    {
        protected ICuentaBancaria CuentaBancaria;

        public CuentaDecorador(ICuentaBancaria cuentaBancaria)
        {
            this.CuentaBancaria = cuentaBancaria;
        }

        public void AbrirCuenta(Cuenta cuenta)
        {
            this.CuentaBancaria.AbrirCuenta(cuenta);
        }
    }
}
