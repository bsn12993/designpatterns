﻿using Patron.Decorator.Interfaz;
using Patron.Decorator.Models;
using System;

namespace Patron.Decorator.Implementacion
{
    public class CuentaCorriente : ICuentaBancaria
    {
        public void AbrirCuenta(Cuenta cuenta)
        {
            Console.WriteLine("====================");
            Console.WriteLine("Se abrió una Cuenta Corriente");
            Console.WriteLine($"Cliente {cuenta.Cliente}");
        }
    }
}
