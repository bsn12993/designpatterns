﻿using Patron.Decorator.Interfaz;
using Patron.Decorator.Models;
using System;

namespace Patron.Decorator.Implementacion
{
    public class CuentaAhorro : ICuentaBancaria
    {
        public void AbrirCuenta(Cuenta cuenta)
        {
            Console.WriteLine("====================");
            Console.WriteLine("Se abrió una Cuenta de Ahorro");
            Console.WriteLine($"Cliente {cuenta.Cliente}");
        }
    }
}
