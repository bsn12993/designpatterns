﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron.Decorator.Models
{
    public class Cuenta
    {
        public int Id { get; set; }
        public string Cliente { get; set; }

        public Cuenta()
        {

        }

        public Cuenta(int id, string cliente)
        {
            this.Id = id;
            this.Cliente = cliente;
        }
    }
}
